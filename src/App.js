import React, { Component } from 'react';
import { Route } from 'react-router-dom'
import Signup from './components/sign-up'
import LoginForm from './components/login-form'
import Navbar from './components/navbar';
import axios from 'axios';
import Home from './components/home';
import readXlsxFile from 'read-excel-file';
import { JsonToTable } from 'react-json-to-table'

const schema = {
  'Sno': {
    prop: 'sno',
    type: Number,
    required: true
  },
  'Name': {
    prop: 'name',
    type: String,
    required: true
  },
  'Age': {
    prop: 'age',
    type: Number,
    required: true
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      username: null,
      name: null,
      mobile: null,
      id: null,
      json: [],
      Uploaded: null
    }
    this.componentDidMount = this.componentDidMount.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.getJSON = this.getJSON.bind(this);
  }


  componentDidMount() {
  }

  updateUser(userObject) {
    this.setState(userObject)
  }

  getJSON(event) {
    readXlsxFile(event.target.files[0], { schema }).then((rows) => {
      this.setState({ json: rows.rows });
      let Obj = {
        'id': this.state.id,
        'json': rows.rows
      }
      axios.post('/user/uploadJSON', Obj).then(response => {
        setTimeout(() => {
          this.setState({ Uploaded: null });
        }, 2000);
        this.setState({ Uploaded: "Uploaded Sucessfully" });
      })
    });
  }

  render() {
    return (
      <div className="App">
        <Navbar updateUser={this.updateUser} loggedIn={this.state.loggedIn} />
        {this.state.loggedIn &&
          <div className="container1">
            <div className="avatar-flip">
              <img src="https://scontent.fhyd7-1.fna.fbcdn.net/v/t1.0-9/16196015_10154888128487744_6901111466535510271_n.png?_nc_cat=103&_nc_ht=scontent.fhyd7-1.fna&oh=e42cc2f9f44e1ae1e7c22edb56570437&oe=5D43E7E9" height="150" width="150" alt="" />
            </div>
            {this.state.loggedIn}
            <h4>Welcome {this.state.name} !</h4>
            <p>User Name:  :{this.state.username}</p>
            <p>Mobile: {this.state.mobile}</p>
            <input type="file" id="input" className="file" onChange={this.getJSON} accept=".xlsx, .xls, .csv" />
            <br />
            {this.state.Uploaded && <div className="valid1">{this.state.Uploaded}</div>}
            {this.state.json && <p className="file1">Uploaded File:</p>}
            <JsonToTable json={this.state.json} />
          </div>
        }
        {!this.state.loggedIn && <Route
          exact path="/"
          component={Home} />}
        <Route
          path="/login"
          render={() =>
            <LoginForm
              updateUser={this.updateUser}
            />}
        />
        <Route
          path="/signup"
          render={() =>
            <Signup />}
        />
      </div>
    );
  }
}

export default App;
