import axios from 'axios'
import React from 'react';
import { Link } from 'react-router-dom';
class Signup extends React.Component {
	constructor() {
		super();
		this.state = {
			fields: {},
			errors: {}
		}

		this.handleChange = this.handleChange.bind(this);
		this.submituserRegistrationForm = this.submituserRegistrationForm.bind(this);

	};

	handleChange(e) {
		let fields = this.state.fields;
		let errors = {};
		fields[e.target.name] = e.target.value;
		this.setState({
			fields,
			errors
		});

	}

	submituserRegistrationForm(e) {
		e.preventDefault();
		let errors = {};
		if (this.validateForm()) {
			let fields = {};
			fields["username"] = "";
			fields["name"] = "";
			fields["mobile"] = "";
			fields["password"] = "";
			this.setState({ fields: fields });
			axios.post('/user/', {
				username: this.state.fields.username,
				password: this.state.fields.password,
				mobile: this.state.fields.mobile,
				name: this.state.fields.name
			}).then(response => {
				if (response.data.code === 200) {
					this.setState({
						redirectTo: '/login'
					});
					errors["valid"] = "Registration Sucessfull!";
				} else {
					errors["invalid"] = "Username already taken";
				}
				this.setState({
					errors: errors
				});
			}).catch(error => {
				errors["invalid"] = "Server Error, please try again later";
				this.setState({
					errors: errors
				});
			})

		}

	}

	validateForm() {

		let fields = this.state.fields;
		let errors = {};
		let formIsValid = true;

		if (!fields["username"]) {
			formIsValid = false;
			errors["username"] = "Please enter your username.";
		}

		if (!fields["name"]) {
			formIsValid = false;
			errors["name"] = "Please enter your name.";
		}

		if (!fields["mobile"]) {
			formIsValid = false;
			errors["mobile"] = "Please enter your mobile no.";
		}

		if (typeof fields["mobile"] !== "undefined") {
			if (!fields["mobile"].match(/^[0-9]{10}$/)) {
				formIsValid = false;
				errors["mobile"] = "Please enter valid mobile no.";
			}
		}

		if (!fields["password"]) {
			formIsValid = false;
			errors["password"] = "Please enter your password.";
		}

		this.setState({
			errors: errors
		});
		return formIsValid;


	}



	render() {
		return (
			<div id="main-registration-container">
				<div id="register">
					<h3 className="heading">Registration</h3>
					<form method="post" name="userRegistrationForm" onSubmit={this.submituserRegistrationForm} >
						<label>Username</label>
						<input type="text" name="username" value={this.state.fields.username ? this.state.fields.username : ''} onChange={this.handleChange} maxLength="30" />
						<div className="errorMsg">{this.state.errors.username}</div>
						<label>Password</label>
						<input type="password" name="password" value={this.state.fields.password ? this.state.fields.password : ''} onChange={this.handleChange} maxLength="30" />
						<div className="errorMsg">{this.state.errors.password}</div>
						<label>Name</label>
						<input type="text" name="name" value={this.state.fields.name ? this.state.fields.name : ''} onChange={this.handleChange} maxLength="30" />
						<div className="errorMsg">{this.state.errors.name}</div>
						<label>Mobile No</label>
						<input type="text" name="mobile" value={this.state.fields.mobile ? this.state.fields.mobile : ''} onChange={this.handleChange} maxLength="10" />
						<div className="errorMsg">{this.state.errors.mobile}</div>
						{this.state.errors.invalid && <div className="user">{this.state.errors.invalid}</div>}
						{this.state.errors.valid && <div className="valid">{this.state.errors.valid}</div>}
						<input type="submit" className="button" value="Register" />
						<div className="pull-right">Or already have an account? <Link to="/login" className="btn btn-link text-secondary active">
							<span className="text-secondary">Log In</span>
						</Link></div>
					</form>
				</div>
			</div>

		);
	}


}


export default Signup;