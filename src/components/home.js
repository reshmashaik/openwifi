import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../index.css';
class Home extends Component {
    render() {
        return (
            <div>
                <h1 className="welcome">Welcome !</h1>
                <Link to="/login" >
                    <button className="button1"><span>Get Started </span></button>
                </Link>
            </div>
        )
    }
}

export default Home
