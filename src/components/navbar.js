import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

class Navbar extends Component {
    constructor() {
        super()
        this.logout = this.logout.bind(this)
    }

    logout(event) {
        event.preventDefault()
        axios.post('/user/logout').then(response => {
            if (response.status === 200) {
                this.props.updateUser({
                    loggedIn: false,
                    username: null,
                    mobile: null,
                    name: null,
                    id: null,
                    json: [],
                })
            }
        }).catch(error => {
        })
    }

    render() {
        const loggedIn = this.props.loggedIn;
        return (
            <div>
                <header id="nav-container">
                    <div className="navbar-section">
                        {loggedIn ? (
                            <section>
                                <Link to="#" className="logout" onClick={this.logout}> <span >logout</span></Link>
                            </section>
                        ) : (
                                <div></div>
                            )}
                    </div>
                </header>
            </div>
        );
    }
}

export default Navbar