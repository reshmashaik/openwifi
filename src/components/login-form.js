import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import axios from 'axios';
import { Link } from 'react-router-dom';

class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            fields: {},
            errors: {}
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)

    }

    handleChange(event) {
        let fields = this.state.fields;
        let errors = {};
        fields[event.target.name] = event.target.value;
        this.setState({
            fields,
            errors
        });
    }

    handleSubmit(event) {
        let errors = {};
        event.preventDefault();
        if (this.validateForm()) {
            let fields = {};
            fields["username"] = '';
            fields["password"] = '';
            this.setState({ fields: fields });
            axios
                .post('/user/login', {
                    username: this.state.fields.username,
                    password: this.state.fields.password
                })
                .then(response => {
                    if (response.status === 200) {
                        this.props.updateUser({
                            loggedIn: true,
                            username: response.data.username,
                            mobile: response.data.mobile,
                            name: response.data.name,
                            id: response.data._id,
                            json: response.data.json
                        })
                        this.setState({
                            redirectTo: '/'
                        })
                    }
                }).catch(error => {
                    errors["invalid"] = "Invalid Credentials";
                    this.setState({
                        errors: errors
                    });
                })
        }
    }

    validateForm() {

        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["username"]) {
            formIsValid = false;
            errors["username"] = "Please enter your username.";
        }

        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Please enter your password.";
        }

        this.setState({
            errors: errors
        });
        return formIsValid;


    }

    render() {
        if (this.state.redirectTo) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        } else {
            return (
                <div>
                    <div id="main-registration-container">
                        <div id="login">
                            <h3 className="heading">Login</h3>
                            <form method="post" name="userRegistrationForm" onSubmit={this.handleSubmit} >
                                <label>Username</label>
                                <input type="text" name="username" value={this.state.fields.username ? this.state.fields.username : ''} onChange={this.handleChange} maxLength="30"/>
                                <div className="errorMsg">{this.state.errors.username}</div>
                                <label>Password</label>
                                <input type="password" name="password" value={this.state.fields.password ? this.state.fields.password : ''} onChange={this.handleChange} maxLength="30"/>
                                <div className="errorMsg">{this.state.errors.password}</div>
                                <div className="errorMsg">{this.state.errors.invalid}</div>
                                <input type="submit" className="buttonlogin" value="Login" />
                                <div className="pull-right">Don't have an account? <Link to="/signup" className="btn btn-link">
                                    <span className="text-secondary">Sign Up</span>
                                </Link></div>
                            </form>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default LoginForm
