const express = require('express')
const router = express.Router()
const User = require('../database/models/user')
const passport = require('../passport')

router.post('/', (req, res) => {

    const { username, password, mobile, name } = req.body

    User.findOne({ username: username }, (err, user) => {
        if (err) {
        } else if (user) {
            res.json({ code: 400 })
        }
        else {
            const newUser = new User({
                username: username,
                password: password,
                name: name,
                mobile: mobile
            })
            newUser.save((err, savedUser) => {
                if (err) return res.json(err)
                res.json({ code: 200 })
            })
        }
    })
})

router.post(
    '/login',
    function (req, res, next) {
        next()
    },
    passport.authenticate('local'),
    (req, res) => {
        var userInfo = {
            username: req.user.username,
            mobile: req.user.mobile,
            name: req.user.name,
            _id: req.user._id,
            json: req.user.json
        };
        res.send(userInfo);
    }
)

router.post('/logout', (req, res) => {
    if (req.user) {
        req.logout()
        res.send({ msg: 'logging out' })
    } else {
        res.send({ msg: 'no user to log out' })
    }
})


router.post('/uploadJSON', (req, res) => {
    User.update({ _id: req.body.id }, { json: req.body.json }, (err, user) => {
        if (err) return res.json(err)
        res.json(user)
    })
})

module.exports = router